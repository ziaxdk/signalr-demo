﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace DemoSignalR
{
    public class UdpReceiver
    {
        private readonly int _port;
        private readonly string _host;
        private readonly Action<byte[]> _cb;
        private UdpClient _udpClient;

        public UdpReceiver(int port, string host, Action<byte[]> cb)
        {
            _port = port;
            _host = host;
            _cb = cb;

            _udpClient = new UdpClient(_port);

            //IPEndPoint remoteEndPoint = new IPEndPoint(IPAddress.Any, 0);
            IPEndPoint remoteEndPoint = GetIPEndPointFromHostName(_host, _port);
            //IPEndPoint remoteEndPoint = new IPEndPoint(IPAddress.Any, _port);

            Console.WriteLine($"Listening for UDP msg on {_host} ({remoteEndPoint}) at {_port}");
            Action<IAsyncResult> callback = null;
            callback = ar =>
            {
                if (_udpClient.Client == null) return;
                var buffer = _udpClient.EndReceive(ar, ref remoteEndPoint);
                _cb(buffer);
                _udpClient.BeginReceive(new AsyncCallback(callback), null);
            };

            try
            {
                _udpClient.BeginReceive(new AsyncCallback(callback), null);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public void Close()
        {
            _udpClient.Close();
        }


        static IPEndPoint GetIPEndPointFromHostName(string hostName, int port)
        {
            var addresses = Dns.GetHostAddresses(hostName);
            if (addresses.Length == 0)
            {
                throw new ArgumentException(
                    "Unable to retrieve address from specified host name.",
                    "hostName"
                );
            }
            else if (addresses.Length > 1)
            {
                throw new ArgumentException(
                    "There is more that one IP address to the specified host.",
                    "hostName"
                );
            }
            return new IPEndPoint(addresses[0], port); // Port gets validated here.
        }
    }
}
