(function() {
  const Home = Vue.component('Home', {
    template: `<div class="pure-menu custom-restricted-width">
      <span class="pure-menu-heading">SignalR demo</span>

      <ul class="pure-menu-list">
          <li class="pure-menu-item"><router-link to="/" class="pure-menu-link">Home</router-link></li>
          <li class="pure-menu-item"><router-link to="/hello" class="pure-menu-link">Hello</router-link></li>
          <li class="pure-menu-item"><router-link to="/chat" class="pure-menu-link">Chat</router-link></li>
          <li class="pure-menu-item"><router-link to="/log" class="pure-menu-link">Log</router-link></li>
      </ul>
  </div>`
  });


  const Hello = Vue.component('Hello', {
    template: `<div>
      <h2>Hello</h2>
      <signalr-status :connected="connected"></signalr-status>
      <p>Press H in backend window</p>
      <ul>
        <li v-for="line in lines">
          <strong v-text="line.name"></strong>
          <span v-text="line.message"></span> (<span v-text="line.time"></span>)
        </li>
      </ul>
    </div>`,
    data() {
      return {
        lines: [],
        connected: false
      }
    },
    mounted() {
    //Set the hubs URL for the connection
      $.connection.hub.url = "/signalr";
      var hub = $.connection.myHub;
      hub.client.addMessage = function (name, message, time) {
        this.lines.push({ name, message, time })
      }.bind(this);

      var _t = this;
      $.connection.hub.start().done(function() {
        _t.connected = true
      });

    },
    destroyed() {

    }
  });

  const Chat = (function() {
    var chatHub;
      return Vue.component('Chat', {
      template: `<div>
        <h2>Chat</h2>
        <signalr-status :connected="connected"></signalr-status>

        <div v-if="!isAuth">
          <h4>You are not authorized to chat</h4>
          <input v-model="name2" @keyup.13="signIn" />
        </div>
        
        <div v-if="isAuth">
          <h4>Hello {{name}}.</h4>

          <a href="javascript:;" class="pure-button pure-button-active" @click="signOut">SignOut</a>

          <ul>
            <li v-for="msg in messages">
              <span v-text="msg.text"></span>
              <strong v-text="msg.user"></strong>
            </li>
          </ul>

          <div>
            <input v-model="message" @keyup.13="onMessage" />
          </div>
        </div>

      </div>`,
      data() {
        return {
          name2: null,
          name: null,
          message: null,
          connected: false,
          messages: []
        }
      },
      computed: {
        isAuth() {
          return this.name !== null;
        }
      },
      methods: {
        signIn() {
          this.name = this.name2;
          chatHub.server.login(this.name);
        },
        signOut() {
          chatHub.server.logout(this.name);
          this.name = null;
        },
        onMessage() {
          chatHub.server.addMessage(this.name, this.message);
          this.message = null;

        }
      },
      mounted() {
      //Set the hubs URL for the connection
        $.connection.hub.url = "/signalr";
        chatHub = $.connection.chat;
        chatHub.client.addMessage = function (text, user) {
          this.messages.push({ text, user });
        }.bind(this);

        var _t = this;
        $.connection.hub.start().done(function() {
          _t.connected = true;
        });
      },
      destroyed() {
        $.connection.hub.stop();
      }
    });
  })();

  const Log = (function() {
    var logHub;
    return Vue.component('Log', {
      template: `<div>
        <h2>Chat</h2>
        <signalr-status :connected="connected"></signalr-status>
        <p>Press L in backend window</p>

        <ul>
          <li v-for="log in logs">
            <span v-text="log.text"></span>
            <strong v-text="log.time"></strong>
          </li>
        </ul>


      </div>`,
      data() {
        return {
          connected: false,
          logs: []
        }
      },
      mounted() {
      //Set the hubs URL for the connection
        $.connection.hub.url = "/signalr";
        logHub = $.connection.log;
        logHub.client.addLog = function (text, time) {
          this.logs.unshift({ text, time });
          if (this.logs.length >= 25) {
            this.logs.pop();
          }
          // this.logs.push({ text, time });
        }.bind(this);

        var _t = this;
        $.connection.hub.start().done(function() {
          _t.connected = true;
        });
      },
      destroyed() {
        $.connection.hub.stop();
      }
    })
  }());

  const SignalRStatus = Vue.component('signalr-status', {
    template: `<div style="height: 5px" :style="{ backgroundColor: color }"></div>`,
    props: {
      connected: {
        type: Boolean,
        default: false
      }
    },
    computed: {
      color() {
        return this.connected ? 'green': 'red';
      }
    }
  })

  const routes = [
    { path: '/hello', component: Hello },
    { path: '/chat', component: Chat },
    { path: '/log', component: Log }
  ]

  const router = new VueRouter({ routes: routes });

  const app = new Vue({ router: router, el: '#app' });
}());