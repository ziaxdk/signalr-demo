﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Hosting;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Owin;
using System.Net;
using Microsoft.Owin.StaticFiles;
using Microsoft.Owin.FileSystems;
using Microsoft.Owin.Extensions;

[assembly: OwinStartup(typeof(DemoSignalR.Startup))]
namespace DemoSignalR
{
    class Program
    {
        private static UdpReceiver udpReceiver;
        static void Main(string[] args)
        {
            string url = "http://localhost:9000";
            using (WebApp.Start(url))
            {
                Console.WriteLine("Server running on {0}", url);
                ConsoleKeyInfo keyInfo = default(ConsoleKeyInfo);
                do
                {
                    keyInfo = Console.ReadKey(true);
                    if (keyInfo.Key == ConsoleKey.H)
                    {
                        var context = GlobalHost.ConnectionManager.GetHubContext<MyHub>();
                        context.Clients.All.addMessage("Hello", "From the backend", DateTime.Now.ToShortTimeString());
                        Console.Write("H");
                    }
                    if (keyInfo.Key == ConsoleKey.C)
                    {
                        var context = GlobalHost.ConnectionManager.GetHubContext<Chat>();
                        context.Clients.All.addMessage("Hello", "keo");
                        Console.Write("C");
                    }
                    else if (keyInfo.Key == ConsoleKey.L)
                    {
                        if (udpReceiver == null)
                        {
                            var port = 8080; // 
                            //var host = "ne-tst-wstcas11";
                            var host = "10.66.100.111";
                            //var host = "127.0.0.1";
                            udpReceiver = new UdpReceiver(port, host, _ =>
                            {
                                var log = Encoding.UTF8.GetString(_);
                                var context = GlobalHost.ConnectionManager.GetHubContext<Log>();
                                context.Clients.All.addLog(log);
                                Debug.WriteLine($"({_.Length}) {log}");
                            });
                        }
                        else
                        {
                            Console.WriteLine("Stopping UdpListener");
                            udpReceiver.Close();
                            udpReceiver = null;
                        }
                    }
                } while (keyInfo.Key != ConsoleKey.Q);
            }
        }

    }
    class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCors(CorsOptions.AllowAll);
            app.UseFileServer(new FileServerOptions()
            {
                FileSystem = new PhysicalFileSystem("..\\..\\app"),
                EnableDefaultFiles = true,
                EnableDirectoryBrowsing = false
            });
            app.MapSignalR();
        }
    }
    public class MyHub : Hub
    {
        public void Send(string name, string message)
        {
            Clients.All.addMessage(name, message);
        }
    }

    public class Chat : Hub
    {
        public void Login(string name)
        {
            var context = GlobalHost.ConnectionManager.GetHubContext<Chat>();
            context.Clients.All.addMessage("User joined chat", name);
        }

        public void Logout(string name)
        {
            var context = GlobalHost.ConnectionManager.GetHubContext<Chat>();
            context.Clients.All.addMessage("User left chat", name);
        }
        public void AddMessage(string name, string message)
        {
            var context = GlobalHost.ConnectionManager.GetHubContext<Chat>();
            context.Clients.All.addMessage(message, name);
        }
    }

    public class Log : Hub
    {
    }
}
